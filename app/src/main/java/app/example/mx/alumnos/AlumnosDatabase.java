package app.example.mx.alumnos;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.support.v4.content.LocalBroadcastManager;



public class AlumnosDatabase extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "alumnos.db";
    private static final String TABLE_NAME = "alumnos";
    public static final String COL_NAME = "nombre";
    public static final String COL_DOMICILIO = "domicilio";
    public static final String COL_CURP = "curp";
    public static final String COL_CARRERA = "carrera";
    public static final String COL_SEMESTRE = "semestre";


    public AlumnosDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String createQuery =
                "CREATE TABLE " + TABLE_NAME +
                        " (_id INTEGER PRIMARY KEY, "
                        + COL_NAME + " TEXT NOT NULL COLLATE UNICODE, "
                        + COL_DOMICILIO + " TEXT NOT NULL, "
                        + COL_CURP + " TEXT NOT NULL, "
                        + COL_CARRERA + " TEXT NOT NULL, "
                        + COL_SEMESTRE + " TEXT NOT NULL)";

    db.execSQL(createQuery);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String upgradeQuery = "DROP TABLE IF EXIST " + TABLE_NAME;
        db.execSQL(upgradeQuery);

    }

    public static long insertaAlumno (Context context, String nombre,  String domicilio, String curp, String carrera, String semestre ){

        SQLiteOpenHelper dbOpenHelper = new AlumnosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        ContentValues valoralumno = new ContentValues();
        valoralumno.put(COL_NAME,nombre);
        valoralumno.put(COL_DOMICILIO,domicilio);
        valoralumno.put(COL_CURP,curp);
        valoralumno.put(COL_CARRERA,carrera);
        valoralumno.put(COL_SEMESTRE,semestre);

        long result = -1L;
        try {
            result = database.insert(TABLE_NAME, null, valoralumno);

            if (result != -1L)
                {
                    LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
                    Intent intentFilter = new Intent(AlumnosLoader.ACTION_RELOAD_TABLE);
                    broadcastManager.sendBroadcast(intentFilter);
                }
            } finally  {
                dbOpenHelper.close();
            }


        return result;

    }

    public static Cursor devuelveTodos(Context context){

        SQLiteOpenHelper dbOpenHelper = new AlumnosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getReadableDatabase();

        return database.query(
                TABLE_NAME,
                new String[]{COL_NAME, COL_DOMICILIO, COL_CURP, COL_CARRERA, COL_SEMESTRE, BaseColumns._ID},
                null, null, null, null,
                COL_NAME+ " ASC");

    }

    public static Cursor devuelveConId(Context context, long identificador){

        SQLiteOpenHelper dbOpenHelper = new AlumnosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getReadableDatabase();

        return database.query(
                TABLE_NAME,
                new String[]{COL_NAME, COL_DOMICILIO, COL_CURP, COL_CARRERA, COL_SEMESTRE, BaseColumns._ID},
                BaseColumns._ID + " = ?",
                new String[]{String.valueOf(identificador)},
                null, null,
                COL_NAME+ " ASC");
    }


    public static int eliminaConId(Context context, long alumnoId) {

        SQLiteOpenHelper dbOpenHelper = new AlumnosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        int resultado = database.delete(
                TABLE_NAME, BaseColumns._ID + "=?",
                new String[] {String.valueOf(alumnoId)});

        if (resultado != 0)
        {
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
            Intent intentFilter = new Intent(AlumnosLoader.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }

        dbOpenHelper.close();
        return resultado;
    }


    public static int actualizaAlumno (Context context, String nombre,  String domicilio, String curp, String carrera, String semestre, long alumnoId){

        SQLiteOpenHelper dbOpenHelper = new AlumnosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        ContentValues valoralumno = new ContentValues();
        valoralumno.put(COL_NAME,nombre);
        valoralumno.put(COL_DOMICILIO,domicilio);
        valoralumno.put(COL_CURP,curp);
        valoralumno.put(COL_CARRERA,carrera);
        valoralumno.put(COL_SEMESTRE,semestre);

        int result = database.update(
                TABLE_NAME,
                valoralumno,
                BaseColumns._ID + " =?",
                new String[]{String.valueOf(alumnoId)});

            if (result != 0){

                LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
                Intent intentFilter = new Intent(AlumnosLoader.ACTION_RELOAD_TABLE);
                broadcastManager.sendBroadcast(intentFilter);
            }

            dbOpenHelper.close();

        return result;

}
}
