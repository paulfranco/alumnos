package app.example.mx.alumnos;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class AgregarAlumno extends AppCompatActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private long alumnoId;
    private EditText alumnoEditText;
    private EditText domicilioEditText;
    private EditText curpEditText;
    private EditText carreraEditText;
    private EditText semestreEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_alumno);

        alumnoEditText = (EditText) findViewById(R.id.nombre_edit_text);
        domicilioEditText = (EditText) findViewById(R.id.domicilio_edit_text);
        curpEditText = (EditText) findViewById(R.id.curp_edit_text);
        carreraEditText = (EditText) findViewById(R.id.carrera_edit_text);
        semestreEditText = (EditText) findViewById(R.id.semestre_edit_text);

        alumnoId = getIntent().getLongExtra(DetalleAlumno.EXTRA_alumno_ID, -1L);

        if (alumnoId != -1L){
            getSupportLoaderManager().initLoader(0, null, this);
        }

        findViewById(R.id.boton_agregar).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        String nombreAlumno = alumnoEditText.getText().toString();
        String domicilioAlumno = domicilioEditText.getText().toString();
        String curpAlumno = curpEditText.getText().toString();
        String carreraAlumno = carreraEditText.getText().toString();
        String semestreAlumno = semestreEditText.getText().toString();

        new CreateAlumnoTask(this, nombreAlumno, domicilioAlumno, curpAlumno, carreraAlumno, semestreAlumno, alumnoId).execute();

    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new AlumnoLoader(this,alumnoId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data != null && data.moveToFirst()){

            int nameIndex = data.getColumnIndexOrThrow(AlumnosDatabase.COL_NAME);
            String nombreAlumno= data.getString(nameIndex);

            int domicilioIndex = data.getColumnIndexOrThrow(AlumnosDatabase.COL_DOMICILIO);
            String domicilioAlumno= data.getString(domicilioIndex);

            int curpIndex = data.getColumnIndexOrThrow(AlumnosDatabase.COL_CURP);
            String curpAlumno= data.getString(curpIndex);

            int carreraIndex = data.getColumnIndexOrThrow(AlumnosDatabase.COL_CARRERA);
            String carreraAlumno= data.getString(carreraIndex);

            int semestreIndex = data.getColumnIndexOrThrow(AlumnosDatabase.COL_SEMESTRE);
            String semestreAlumno= data.getString(semestreIndex);

            alumnoEditText.setText(nombreAlumno);
            domicilioEditText.setText(domicilioAlumno);
            curpEditText.setText(curpAlumno);
            carreraEditText.setText(carreraAlumno);
            semestreEditText.setText(semestreAlumno);

        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public static class CreateAlumnoTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        private String alumnoName;
        private String alumnoDomi;
        private String alumnoCurp;
        private String alumnoCarr;
        private String alumnoSeme;

        private long alumnoId;

        public CreateAlumnoTask(Activity activity, String name, String domi, String curp, String carr, String seme, long alumnoId){
            weakActivity = new WeakReference<Activity>(activity);
            alumnoName = name;
            alumnoDomi = domi;
            alumnoCurp = curp;
            alumnoCarr = carr;
            alumnoSeme = seme;

            this.alumnoId = alumnoId;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }

            Context appContext = context.getApplicationContext();

            Boolean success = false;

            if (alumnoId != -1L) {
                int filasAfectadas = AlumnosDatabase.actualizaAlumno(appContext, alumnoName, alumnoDomi, alumnoCurp, alumnoCarr, alumnoSeme, alumnoId);
                success = (filasAfectadas !=0);
            } else {
                long id = AlumnosDatabase.insertaAlumno(appContext, alumnoName, alumnoDomi, alumnoCurp, alumnoCarr, alumnoSeme);
                success = (id != -1L);
            }
            return success;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "Grabacion Correcta", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "No se guardo", Toast.LENGTH_SHORT).show();
            }
            context.finish();
        }
    }

}