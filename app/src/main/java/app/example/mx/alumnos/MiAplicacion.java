package app.example.mx.alumnos;

import android.app.Application;

import com.facebook.stetho.Stetho;


public class MiAplicacion extends Application {

    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }


}

