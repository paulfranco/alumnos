package app.example.mx.alumnos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class DetalleAlumno extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private TextView nombreTextView;
    private TextView domicilioTextView;
    private TextView curpTextView;
    private TextView carreraTextView;
    private TextView semestreTextView;

    private long alumnoId;
    public static final String EXTRA_alumno_ID = "alumno.id.extra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_alumno);



        nombreTextView = (TextView) findViewById(R.id.nombre_text_view);
        domicilioTextView = (TextView) findViewById(R.id.domicilio_text_view);
        curpTextView = (TextView) findViewById(R.id.curp_text_view);
        carreraTextView = (TextView) findViewById(R.id.carrera_text_view);
        semestreTextView = (TextView) findViewById(R.id.semestre_text_view);

        Intent intencion = getIntent();
        alumnoId = intencion.getLongExtra(MainActivity.EXTRA_ID_alumno, -1L);

        getSupportLoaderManager().initLoader(0, null, this);

        findViewById(R.id.boton_eliminar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new DeletealumnoTask(DetalleAlumno.this,alumnoId).execute();

                    }
                }
        );

        findViewById(R.id.boton_actualizar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent =new Intent(DetalleAlumno.this, AgregarAlumno.class);
                        intent.putExtra(EXTRA_alumno_ID, alumnoId);
                        startActivity(intent);


                    }
                }
        );


    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new AlumnoLoader(this,alumnoId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data != null && data.moveToFirst()){

            int nameIndex = data.getColumnIndexOrThrow(AlumnosDatabase.COL_NAME);
            String nombrealumno= data.getString(nameIndex);

            int domicilioIndex = data.getColumnIndexOrThrow(AlumnosDatabase.COL_DOMICILIO);
            String domicilioalumno= data.getString(domicilioIndex);

            int curpIndex = data.getColumnIndexOrThrow(AlumnosDatabase.COL_CURP);
            String curpalumno= data.getString(curpIndex);

            int carreraIndex = data.getColumnIndexOrThrow(AlumnosDatabase.COL_CARRERA);
            String carreraalumno= data.getString(carreraIndex);

            int semestreIndex = data.getColumnIndexOrThrow(AlumnosDatabase.COL_SEMESTRE);
            String semestrealumno= data.getString(semestreIndex);

            nombreTextView.setText(nombrealumno);
            domicilioTextView.setText(domicilioalumno);
            curpTextView.setText(curpalumno);
            carreraTextView.setText(carreraalumno);
            semestreTextView.setText(semestrealumno);


        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }



    public static class DeletealumnoTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        private long alumnoId;

        public DeletealumnoTask(Activity activity, long id){
            weakActivity = new WeakReference<Activity>(activity);
            alumnoId = id;

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }

            Context appContext = context.getApplicationContext();

            int filasAfectadas = AlumnosDatabase.eliminaConId(appContext, alumnoId);
            return  (filasAfectadas != 0);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "Eliminacion Correcta", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "No se guardo", Toast.LENGTH_SHORT).show();
            }
            context.finish();
        }
    }
}